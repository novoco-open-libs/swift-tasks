import ShellOut
import ArgumentParser

struct Tasks: ParsableCommand {
    static let configuration = CommandConfiguration(
        commandName: "tasks",
        abstract: "An automation task runner for BimutableHashKVStore.",
        subcommands: [Linting.self, Building.self, Testing.self]
    )
}

Tasks.main()

func runShell(_ command: String, continueOnError: Bool = false) throws -> String {
    do {
        return try shellOut(
            to: command,
            outputHandle: .standardOutput,
            errorHandle: .standardError
        )
    } catch {
        if !continueOnError {
            print(error)
            throw ShellError()
        }
    }
    
    return ""
}

struct ShellError: Error, CustomStringConvertible {
    var description: String {
        "Failed when running shell command"
    }
}
