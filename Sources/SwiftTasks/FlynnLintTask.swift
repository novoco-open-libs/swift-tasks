import ArgumentParser

extension Tasks {
    struct Linting: ParsableCommand {
        static var configuration = CommandConfiguration(
            commandName: "lint",
            abstract: "Run FlynLint on the codebase, to generate public async methods."
        )

        func run() throws {
            #if os(Linux)
                try _ = runShell(".build/checkouts/flynn/meta/FlynnLint-linux-x86_64 Sources")
            #else
                try _ = runShell(".build/checkouts/flynn/meta/FlynnLint Sources")
            #endif
        }
    }
}
