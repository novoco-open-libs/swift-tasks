import ArgumentParser

extension Tasks {
    struct Testing: ParsableCommand {
        static var configuration = CommandConfiguration(
            commandName: "test",
            abstract: "Run the unit teast against the project"
        )

        func run() throws {
            print("run() function called")
            #if os(Linux)
                try _ = runShell(".build/checkouts/flynn/meta/FlynnLint-linux-x86_64 Sources", continueOnError: true)
            #else
                try _ = runShell(".build/checkouts/flynn/meta/FlynnLint Sources", continueOnError: true)
            #endif
            
            #if os(Linux)
                try _ = runShell(".build/checkouts/flynn/meta/FlynnLint-linux-x86_64 Tests", continueOnError: true)
            #else
                try _ = runShell(".build/checkouts/flynn/meta/FlynnLint Tests", continueOnError: true)
            #endif
            
            try _ = runShell("swift test")
        }
    }
}


