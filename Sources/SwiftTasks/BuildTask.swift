import ArgumentParser

extension Tasks {
    struct Building: ParsableCommand {
        static var configuration = CommandConfiguration(
            commandName: "build",
            abstract: "Build project"
        )

        func run() throws {
            #if os(Linux)
                try _ = runShell(".build/checkouts/flynn/meta/FlynnLint-linux-x86_64 Sources", continueOnError: true)
            #else
                try _ = runShell(".build/checkouts/flynn/meta/FlynnLint Sources", continueOnError: true)
            #endif
            try _ = runShell("swift build")
        }
    }
}

