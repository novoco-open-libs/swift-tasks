import XCTest

final class SwiftTasksTests: XCTestCase {
    func testPlaceholder() {
        XCTAssertEqual("Hello, World!", "Hello, World!")
    }
    
    static var allTests = [
        ("testPlaceholder", testPlaceholder),
    ]
}
