import XCTest

import SwiftTasksTests

var tests = [XCTestCaseEntry]()
tests += SwiftTasksTests.allTests()
XCTMain(tests)
