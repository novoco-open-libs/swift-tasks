// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftTasks",
    platforms: [
        .macOS(.v10_15),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .executable(
            name: "task",
            targets: ["SwiftTasks"]
        )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(
            name: "swift-argument-parser",
            url: "https://github.com/apple/swift-argument-parser",
            from: "0.3.1"
        ),
        .package(
            name: "ShellOut",
            url: "https://github.com/JohnSundell/ShellOut.git",
            from: "2.3.0"
        ),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SwiftTasks",
            dependencies: [.product(name: "ArgumentParser", package: "swift-argument-parser"), "ShellOut"]
        ),
        .testTarget(
            name: "SwiftTasksTests",
            dependencies: ["SwiftTasks"]
        ),
    ]
)
